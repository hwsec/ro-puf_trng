# RO-PUF4/TRNG Test System --- Pynq-Z2 Board (SD card image: v3.0.1)* #

----------
This project contains the tests systems and applications used to validate and evaluate the performance in a Pynq-Z2 development board of the RO-PUF/TRNG IP module proposed in:

<b><i>On-Line Evaluation and Monitoring of Security Features of an RO-based PUF/TRNG for IoT devices</i></b> <br>
by <b>Luis F. Rojas-Muñoz, S. Sánchez-Solano, M. C. Martínez-Rodríguez and P. Brox</b> <br>
<b>Sensors</b> 2023, 23, 4070. 
<a href="https://doi.org/10.3390/s23084070"> https://doi.org/10.3390/s23084070

----------

### Directory structure ###

- bin: binary files
- bit: bitstream for Pynq-Z2 board
- out: output files
- run: shellscripts for applications
- README.md: this file 

### Related links ###

<ul> 
<li><a href="https://www.tul.com.tw/productspynq-z2.html">TUL Pynq-Z2 Board</a></li>
<li><a href="http://www.pynq.io/">PYNQ: Python Productivity for zynq</a></li>
<li><a href="https://github.com/mesham/pynq_api">C API for PYNQ FPGA board</a></li>
</ul>

### Instalation and use ###

- Install <b>C-API</b> in the SD of Pynq-Z2 board
- Copy this distribution to the SD and unzip it with: <b><i> $ tar xvf  RO_puf-trng.tar.xz</i></b>
- Go to "<b><i>RO_puf-trng"</i></b> directory
- Become <b>root</b> to run the applications and shellscripts
- Define search path with: <b><i># PATH="./bin:$PATH"</i></b>
- Set user limits (for <i>puf_bitselect</i>) with: <b><i># ulimit -s unlimited</i></b>

### Applications ###

#### *puf_bitselect* ####

Executes a series of tests for each of the PUFs instantiated in the Test  System and extracts the metrics that allow selecting (characterization mode) or analyzing (operation mode) the bits of the counters that form the PUF output.
<br>(run command ***ulimit -s unlimited*** before using this app)

	Usage: puf_bitselect [ -h | [-i] [-o] [-s] [-g] [-n] [-l] [-b <board_id>] [-d <dbg_level>]
                         [-c <n_cmpc>] [-t <n_tests>] [-r <n_run>] ]

#### *puf_getdata* ####

Runs successive series of tests for each of the PUFs instantiated in the
Test System in order to verify their correct operation and capture data for off-line evaluation of their characteristics.

	Usage: puf_getdata [ -h | [-i] [-o] [-e] [-g] [-n] [-l] [-b <board_id>] [-c <n_cmpc>]
                       [-d <dbg_level>] [-r <n_runs>] [-t <n_tests>] ]

#### *puf_enrollment* ####

 Executes enrollment processes of the PUFs instantiated in the Test System to obtain their Reference Outputs. Optionally, during each enrollment stage, can also be generated a Selection Mask indicating the challenges (pairs of ROs) with the worst responses from the stability point of view, so that they can be eliminated from the PUF output in order to improve its reliability. 

	Usage: puf_enrollment [ -h | [-i] [-g] [-n] [-l] [-b <board_id>] [-d <dbg_level>]
                          [-c <n_cmpc>] [-e <n_cmps>] [-t <n_tests>] [-r <n_runs>] ]

#### *puf_HDintra* ####

Evaluates, for each of the PUFs instantiated in the Test System, the Hamming Distance with respect to its Reference Output (***HDintra***) for successive runs. To do this, first an enrollment process is performed for each PUF to obtain the reference output and the RO Selection Mask that indicates the discarded comparisons.

	Usage: puf_HDintra [ -h | [-g] [-n] [-l] [-a] [-i] [-k] [-b <board_id>] [-d <dbg_level>] [-c <n_cmpc>]
                       [-e <n_cmps>] [-m <n_tests>] [-t <n_tests>] [-r <n_runs>] [-w <w_time>] ]

#### *puf_HDinter* ####

Evaluates, for each of the PUFs instantiated in the Test System, the Hamming Distance with respect to the other PUFs (***HDinter***) for successive runs. To do this, first an enrollment process is performed for each PUF to obtain the Reference Output and the RO Selection Mask that indicates the discarded comparisons.

	Usage: puf_HDinter [ -h | [-g] [-n] [-l] [-a] [-i] [-b <board_id>] [-d <dbg_level>] [-c <n_cmpc>]
                       [-e <n_cmps>] [-m <n_tests>] [-t <n_tests>] [-r <n_runs>] ]


#### *puf_reliability* ####

Evaluates the reliability of the PUFs instantiated in the Test System. To do this, an enrollment process is first performed for each PUF to obtain its Reference Output. Subsequently, the Key Masks obtained by applying an ECC with a given repetition code to the responses of the successive series of invocations to the PUF are analyzed.

	Usage: puf_reliability [ -h | [-i] [-o] [-g] [-n] [-l] [-b <board_id>] [-d <dbg_level>]
                           [-c <n_cmpc>] [-e <n_cmpc>] [-m <n_tests>] [-t <n_tests>]
                           [-p <ecc_repcode>] [-r <n_runs>] ]

#### *puf_uniqueness* ####

Evaluates the uniqueness of the PUFs instantiated in the Test System. To do this, an enrollment process is first performed for each PUF to  obtain its Reference Output. Subsequently, the Key Masks obtained by applying an ECC with a given repetition code to the responses of the successive series of  invocations to the other PUFs are analyzed.

	Usage: puf_uniqueness [ -h | [-i] [-o] [-g] [-n] [-l] [-b <board_id>] [-d <dbg_level>]
                          [-c <n_cmpc>] [-e <n_cmpc>] [-m <n_tests>] [-t <n_tests>]
                          [-p <ecc_repcode>] [-r <n_runs>] ]

#### *puf_test* ####

For each of the PUFs instantiated in the Test System: 1) Processes input parameters to calculate the number of challenges that can be discarded; 2) Executes an enrollment processes to obtain the PUF Reference Output and the Challenges Selection Mask; 3) Evaluates HDintra metric after applyng the challenges selection estrategy; and 4) Invokes the PUF repeatedly to verify its reliability as an ID generator using the chosen configuration.

	Usage: puf_test [ -h | [-g] [-n] [-l] [-d <dbg_level>]
                    [-c <n_cmpc>] [-k <n_bits>] [-p <ecc_repcode>]
                    [-m <n_tests>] [-t <n_tests>] [-r <n_runs>] ]

#### *trng_getdata* ####

Collects 10^6 random bits by running 1000 tests for the PUF instantiated  in the System. This amount of bits is reached by performing 250 ROs comparisons and making use of the PUF's TRNG functionality.

	Usage: trng_getdata [ -h | [-g] [-n] [-i <puf_instance>]
                        [-b <board_id>] [-d <dbg_level>] ]

#### *trng_validation* ####

Detects whether the entropy source is presenting a failure by evaluating the 2 health tests (Repetition Count and Adaptative Propotion) presented in the NIST recommendation 800-90b. The parameters required to evaluate these tests were calculated during the process of characterizing the PUF as an entropy source. 

 	Usage: trng_validation [ -h | [-f <filename.bin>] ]

### Shellscripts ###

	# ./run/Bitselect.sh
	# ./run/Enrollment.sh
	# ./run/HDintra.sh	
	# ./run/HDinter.sh
	# ./run/Reliability.sh
	# ./run/Uniqueness.sh
	# ./run/ES_validation.sh

------

 * *Required libraries:<br> 
 	- libc.so.6
 	- libm.so.6
